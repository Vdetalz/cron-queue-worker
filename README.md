# CRON queue worker
A test module that sends a welcome email to registered users
Used the Cron Queue by adding to the queue the user id
whenever a user registers
Created a queue worker that picks these queue items (uids)
during the cron and sends a welcome email (can be any simple
text) to the registered email address.
