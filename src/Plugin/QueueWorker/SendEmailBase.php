<?php
/**
 * @file
 * Contains Drupal\welcome_email\Plugin\QueueWorker\SendEmailBase.php
 */

namespace Drupal\welcome_email\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides base functionality for the SendEmailBase Queue Workers.
 */
abstract class SendEmailBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Creates a new SendEmailBase object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $user_storage
   *   The user storage.
   */
  public function __construct(EntityStorageInterface $user_storage) {
    $this->userStorage = $user_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('user')
    );
  }

  /**
   * Get a user email.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return string
   *   user email
   */
  protected function getUserEmail(UserInterface $user) {
    return $user->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var UserInterface $user */
    $user = $this->userStorage->load($data->uid);
    if (!$user->isNew() && $user instanceof UserInterface) {
      $this->sendUserEmail($user);
    }
  }

  /**
   * Returns service manager.mail.
   *
   * @return mixed
   */
  private function mailManager() {
    /** @var \Drupal\Core\Mail\MailManagerInterface */
    return \Drupal::service('plugin.manager.mail');
  }

  /**
   * Sends email registered users.
   *
   * @param \Drupal\user\UserInterface $user
   */
  private function sendUserEmail(UserInterface $user) {
    $module = 'welcome_email';
    $key = $module . '_user_register';
    $to = $this->getUserEmail($user);
    $langcode = $user->getPreferredLangcode();
    $params = [
      'account' => $to,
      'subject' => t('Test message'),
      'body'    => t('Welcome @user module works fine!', ['@user' => $user->getDisplayName()]),
    ];
    $send = TRUE;
    $mailManager = $this->mailManager();
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    if ($result['result'] != TRUE) {
      $message = t('There was a problem sending your email notification to @email.', array('@email' => $to));
      drupal_set_message($message, 'error');
      \Drupal::logger('mail-log')->error($message);
      return;
    }

    $message = t('An email notification has been sent to @email', array('@email' => $to));
    drupal_set_message($message);
    \Drupal::logger('mail-log')->notice($message);
  }

}
