<?php
namespace Drupal\welcome_email\Plugin\QueueWorker;

/**
 * A User Send Email that registered on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_user_welcome_email",
 *   title = @Translation("Cron User Send Welcome Email"),
 *   cron = {"time" = 10}
 * )
 */
class CronUserSendEmail extends SendEmailBase {}
